﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.Statistics.Models.Markov;
using Accord.Statistics.Models.Markov.Learning;
using Accord.Statistics.Models.Markov.Topology;
using Accord.Statistics.Distributions.Univariate;

namespace HMM_Projeto
{
    class MyHMM
    {
        private int states;
        private int symbols;

        public MyHMM(int states, int symbols)
        {
            this.states = states;
            this.symbols = symbols;
        }

        public double[] LearnAndDecide(int[][] input, int[][]teste)
        {
            try
            {

                // Cria um modelo de Markov com m estados e n simbolos
                var hmm = HiddenMarkovModel.CreateDiscrete(this.states, this.symbols);

                // Otimiza o modelo ate atingir uma diferenca de 0.0001 no aprendizado
                var teacher = new BaumWelchLearning<GeneralDiscreteDistribution, int>(hmm)
                {
                    Tolerance = 0.0001,
                    MaxIterations = 0
                };

                // Aprende com as entradas
                teacher.Learn(input);

                double[] prob = new double[teste.Length];
                for (int i = 0; i < teste.Length; i++)
                {
                    // Guarda os valores gerados para as instancias de teste
                    prob[i] = Math.Exp(hmm.LogLikelihood(teste[i]));
                }

                return prob;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
    }
}
