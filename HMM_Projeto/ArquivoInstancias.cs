﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMM_Projeto
{
    class ArquivoInstancias
    {
        protected int n_instancias_treino;
        protected int n_instancias_teste;
        protected string arquivoTreino;
        protected string arquivoTeste;
        protected int n_estados;

        public ArquivoInstancias()
        {
            this.n_instancias_treino = 0;
            this.n_instancias_teste = 0;
            this.arquivoTreino = "";
            this.arquivoTeste = "";
            this.n_estados = 0;
        }

        public int InstanciasTreino
        {
            get { return this.n_instancias_treino; }
            set { this.n_instancias_treino = value; }
        }

        public string ArquivoTreino
        {
            get { return this.arquivoTreino; }
            set { this.arquivoTreino = value; }
        }

        public int InstanciasTeste
        {
            get { return this.n_instancias_teste; }
            set { this.n_instancias_teste = value; }
        }

        public string ArquivoTeste
        {
            get { return this.arquivoTeste; }
            set { this.arquivoTeste = value; }
        }

        public int Estados
        {
            get { return this.n_estados; }
            set { this.n_estados = value; }
        }

        public int[][] Le_Arquivo_Treino()
        {
            int[][] instancias = new int[this.n_instancias_treino][];
            
            for(int i = 0; i < instancias.Length; i++)
            {
                instancias[i] = new int[45];
            }
            
            try
            {
                System.IO.StreamReader arq = new System.IO.StreamReader(@arquivoTreino);
                
                // vetor de 3 dimensoes = instancias, pontos, valores (x, y)
                double[][][] sequencias = new double[this.n_instancias_treino][][];

                string linha = "";
                while (true)
                {
                    for (int j = 0; j < this.n_instancias_treino; j++)
                    {
                        linha = arq.ReadLine();
                        if (linha != null)
                        {
                            sequencias[j] = new double[45][];
                            string[] dados = linha.Split(',');
                            int contador_pontos = 0;
                            for (int i = 0; i < dados.Length - 1; i = i + 2)
                            {
                                // fazendo parse para double
                                double p1 = double.Parse(dados[i].Replace('.', ','));
                                double p2 = double.Parse(dados[i + 1].Replace('.', ','));
                                double[] ponto = new double[] { p1, p2 };

                                sequencias[j][contador_pontos] = ponto;
                                contador_pontos++;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    break;
                }

                /* Rotina para obter os pontos referencias */
                double[] extremos = GetExtremos(sequencias);
                double x_diff = (extremos[0] - extremos[2])/this.n_estados;
                double y_diff = (extremos[1] - extremos[3])/this.n_estados;
                double[][] pontos_ref = new double[this.n_estados][];

                pontos_ref[0] = new double[]{ extremos[2], extremos[3] }; // comecam do menor
                for (int i = 1; i < this.n_estados; i++)
                {
                    pontos_ref[i] = new double[] { (pontos_ref[i - 1][0] + x_diff), (pontos_ref[i - 1][1] + y_diff) };
                }

                /* Agora cada ponto de instancia sera transformado para um ponto referencia */
                /* O indice de pontos_ref sera usado como label da instancia                */
                
                for (int i = 0; i < sequencias.Length; i++)
                {
                    for (int j = 0; j < sequencias[i].Length; j++)
                    {
                        int estado = CalculaEstado(pontos_ref, sequencias[i][j][0], sequencias[i][j][1]);
                        instancias[i][j] = estado; // setando estado para o ponto
                    }
                }
                arq.Close();
                return instancias;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public int[][] Le_Arquivo_Teste()
        {
            int[][] instancias = new int[this.n_instancias_teste][];

            for (int i = 0; i < instancias.Length; i++)
            {
                instancias[i] = new int[45];
            }

            try
            {
                System.IO.StreamReader arq = new System.IO.StreamReader(@arquivoTeste);

                // vetor de 4 dimensoes = classes, instancias, pontos, valores (x, y)
                double[][][] sequencias = new double[this.n_instancias_teste][][];

                string linha = "";
                while (true)
                {
                    for (int j = 0; j < this.n_instancias_teste; j++)
                    {
                        linha = arq.ReadLine();
                        if (linha != null)
                        {
                            sequencias[j] = new double[45][];
                            string[] dados = linha.Split(',');
                            int contador_pontos = 0;
                            for (int i = 0; i < dados.Length - 1; i = i + 2)
                            {
                                // fazendo parse para double
                                double p1 = double.Parse(dados[i].Replace('.', ','));
                                double p2 = double.Parse(dados[i + 1].Replace('.', ','));
                                double[] ponto = new double[] { p1, p2 };

                                sequencias[j][contador_pontos] = ponto;
                                contador_pontos++;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    break;
                }

                /* Rotina para obter os pontos referencias */
                double[] extremos = GetExtremos(sequencias);
                double x_diff = (extremos[0] - extremos[2]) / this.n_estados;
                double y_diff = (extremos[1] - extremos[3]) / this.n_estados;
                double[][] pontos_ref = new double[this.n_estados][];

                pontos_ref[0] = new double[] { extremos[2], extremos[3] }; // comecam do menor
                for (int i = 1; i < this.n_estados; i++)
                {
                    pontos_ref[i] = new double[] { (pontos_ref[i - 1][0] + x_diff), (pontos_ref[i - 1][1] + y_diff) };
                }

                /* Agora cada ponto de instancia sera transformado para um ponto referencia */
                /* O indice de pontos_ref sera usado como label da instancia                */

                for (int i = 0; i < sequencias.Length; i++)
                {
                    for (int j = 0; j < sequencias[i].Length; j++)
                    {
                        int estado = CalculaEstado(pontos_ref, sequencias[i][j][0], sequencias[i][j][1]);
                        instancias[i][j] = estado; // setando estado para o ponto
                    }
                }

                return instancias;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        private double[] GetExtremos(double[][][] sequencias)
        {
            double[] extremos = new double[4];

            try
            {
                extremos[0] = 0.5; // maior x
                extremos[1] = 0.5; // maior y
                extremos[2] = 0.5; // menor x
                extremos[3] = 0.5; // menor y
                
                // encontrar extremos
                for (int i = 0; i < sequencias.Length; i++)
                {
                    for (int j = 0; j < sequencias[i].Length; j++)
                    {
                        // se x maior que x maior
                        if (sequencias[i][j][0] > extremos[0])
                        {
                            extremos[0] = sequencias[i][j][0];
                        }
                        // se y maior que y maior
                        if (sequencias[i][j][1] > extremos[1])
                        {
                            extremos[1] = sequencias[i][j][1];
                        }
                        // se x menor que x menor
                        if (sequencias[i][j][0] < extremos[2])
                        {
                            extremos[2] = sequencias[i][j][0];
                        }
                        // se y menor que y menor
                        if (sequencias[i][j][1] < extremos[3])
                        {
                            extremos[3] = sequencias[i][j][1];
                        }
                    }
                }
                return extremos;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        private int CalculaEstado(double[][] pontos_ref, double x1, double y1)
        {
            try
            {
                int estado = -1;
                double menor_distancia = 9999.99;
                double distancia = 0.0;

                for (int i = 0; i < pontos_ref.Length; i++)
                {
                    // se a distancia euclidiana for menor que a menor
                    distancia = Math.Sqrt((Math.Pow((x1 - pontos_ref[i][0]), 2) + Math.Pow((y1 - pontos_ref[i][1]), 2)));
                    if (distancia < menor_distancia)
                    {
                        menor_distancia = distancia;
                        estado = i;
                    }
                }

                return estado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

        }
    }
}
