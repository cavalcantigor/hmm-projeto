﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.Statistics.Models.Markov;
using Accord.Math;
using Accord.Statistics.Models.Markov.Topology;
using Accord.Statistics.Distributions.Univariate;
using Accord.Statistics.Models.Markov.Learning;
using Accord.Statistics.Distributions.Multivariate;

namespace HMM_Projeto
{
    class Program
    {
        static void Main(string[] args)
        {
            
            ArquivoInstancias arq_hmm_1 = new ArquivoInstancias();
            arq_hmm_1.Arquivo = @"C:\movement_libras.csv";
            arq_hmm_1.Classes = 1;
            arq_hmm_1.Estados = 3;
            arq_hmm_1.Instancias = 24;

            int[][] teste = arq_hmm_1.Le_Arquivo_Teste(@"C:\movement_libras_1.data", arq_hmm_1.Classes, 3)[0];
            int[] labels_teste = new int[] { 1,1,1,2,2,2,3,3,3 };

            Console.WriteLine("HMM_1");
            MyHMM hmm_1 = new MyHMM(12, arq_hmm_1.Estados);
            int[][] hmm_1_treino = arq_hmm_1.Le_Arquivo_Treino()[0];
            double[] prob_hmm_1 = hmm_1.LearnAndDecide(hmm_1_treino, teste);

            ArquivoInstancias arq_hmm_2 = new ArquivoInstancias();
            arq_hmm_2.Arquivo = @"C:\movement_libras_2.csv";
            arq_hmm_2.Classes = 1;
            arq_hmm_2.Estados = 3;
            arq_hmm_2.Instancias = 24;

            Console.WriteLine("HMM_2");
            MyHMM hmm_2 = new MyHMM(12, arq_hmm_2.Estados);
            int[][] hmm_2_treino = arq_hmm_2.Le_Arquivo_Treino()[0];
            double[] prob_hmm_2 = hmm_2.LearnAndDecide(hmm_2_treino, teste);

            for (int i = 0; i < teste.Length; i++)
            {
                Console.Write("Instancia " + (i+1) + ": ");
                for (int j = 0; j < teste[i].Length; j++)
                {
                    Console.Write(teste[i][j] + " ");
                }
                Console.WriteLine();
                Console.Write("HMM_1: " + prob_hmm_1[i].ToString("0.00000000000000000") + "       ");
                Console.Write("HMM_2: " + prob_hmm_2[i].ToString("0.00000000000000000"));
                Console.WriteLine();
            }
            Console.Read();
        }

        static void Exemplo_Pontos()
        {
            double[][][] sequences = Ler_Arquivo_Treino();

            // Specify a initial normal distribution for the samples.
            var density = new MultivariateNormalDistribution(dimension: 2);

            // Creates a continuous hidden Markov Model with two states organized in a forward
            //  topology and an underlying univariate Normal distribution as probability density.
            var model = new HiddenMarkovModel<MultivariateNormalDistribution, double[]>(new Ergodic(2), density);

            // Configure the learning algorithms to train the sequence classifier until the
            // difference in the average log-likelihood changes only by as little as 0.0001
            var teacher = new BaumWelchLearning<MultivariateNormalDistribution, double[]>(model)
            {
                Tolerance = 0.0001,
                MaxIterations = 0,
            };

            // Fit the model
            teacher.Learn(sequences);

            double logLikelihood = teacher.LogLikelihood;

            double[][][] instancias_teste = Ler_Arquivo_Teste();

            // See the likelihood of the sequences learned
            double a1 = Math.Exp(model.LogLikelihood(instancias_teste[0]));
            double a2 = Math.Exp(model.LogLikelihood(instancias_teste[1]));
            double a3 = Math.Exp(model.LogLikelihood(instancias_teste[2]));

            Console.WriteLine("Probabilidade a1: " + a1.ToString());
            Console.WriteLine("Probabilidade a2: " + a2.ToString());
            Console.WriteLine("Probabilidade a3: " + a3.ToString());
        }

        static void Exemplo_Continuo()
        {
            double[][] sequences = new double[][]
            {
                new double[] { 0.79691,0.38194,0.79691,0.37731,0.79884,0.37731,0.79497,0.37731,0.77563,0.35417,0.73501,0.32639,0.67505,0.30093,0.59381,0.29398,0.4971,0.32407,0.41586,0.38426,0.34816,0.45833,0.2882,0.57639,0.26499,0.72685,0.26112,0.82639,0.26306,0.81944,0.27079,0.72685,0.2882,0.60648,0.33075,0.50694,0.38685,0.42593,0.45261,0.37037,0.53965,0.3287,0.62089,0.31481,0.68472,0.32176,0.73308,0.34028,0.76402,0.37037,0.77756,0.37963,0.76015,0.36806,0.70793,0.32407,0.62863,0.29861,0.53965,0.30787,0.44487,0.35185,0.3617,0.4213,0.29787,0.53241,0.2824,0.64815,0.294,0.74537,0.31141,0.83565,0.31335,0.87963,0.30561,0.83796,0.29594,0.74074,0.30368,0.62269,0.34043,0.51389,0.39845,0.42593,0.47389,0.36111,0.55899,0.3125,0.6383,0.29398 },
                new double[] { 0.67892,0.27315,0.68085,0.27315,0.68085,0.27315,0.68085,0.27315,0.67892,0.26852,0.66344,0.25694,0.6383,0.24769,0.59961,0.24074,0.54159,0.23843,0.47195,0.25231,0.39652,0.28009,0.33075,0.32407,0.27079,0.39583,0.2147,0.48611,0.18182,0.58796,0.16248,0.66667,0.14894,0.72685,0.14507,0.75694,0.14894,0.74074,0.16054,0.68056,0.18375,0.59491,0.23404,0.49537,0.29014,0.40741,0.3617,0.33565,0.43907,0.28704,0.51644,0.25926,0.59381,0.25,0.66151,0.26852,0.706,0.29398,0.73694,0.31944,0.74275,0.31944,0.72921,0.30324,0.69826,0.27315,0.64603,0.24769,0.57447,0.23148,0.4913,0.2338,0.41586,0.2662,0.34623,0.32639,0.28627,0.40509,0.23404,0.48843,0.19536,0.57407,0.17795,0.63657,0.17215,0.67361,0.17021,0.69213,0.17215,0.69213 },
                new double[] { 0.72147,0.23611,0.7234,0.23611,0.7234,0.23611,0.7234,0.23611,0.7234,0.23611,0.71567,0.2338,0.67505,0.22685,0.59188,0.22685,0.4971,0.25,0.42747,0.3125,0.36944,0.38194,0.30948,0.49306,0.27853,0.62269,0.26306,0.71528,0.26112,0.76389,0.25919,0.7662,0.27466,0.69676,0.30754,0.55787,0.37718,0.43056,0.46035,0.35185,0.55899,0.30556,0.65377,0.29861,0.73114,0.2963,0.79304,0.30556,0.82012,0.31481,0.80851,0.30787,0.75629,0.28704,0.66538,0.27083,0.54932,0.26157,0.45841,0.31481,0.39458,0.3912,0.33849,0.48843,0.30754,0.59722,0.2882,0.6713,0.28046,0.71759,0.28046,0.73148,0.2882,0.6875,0.31141,0.58102,0.3675,0.46759,0.44101,0.37731,0.52031,0.30556,0.59768,0.25926,0.67118,0.25231,0.73501,0.2662,0.78143,0.27778 },
                new double[] { 0.5648,0.32407,0.56286,0.32407,0.56093,0.32407,0.55899,0.32407,0.55899,0.32407,0.55126,0.31019,0.52224,0.28472,0.46615,0.26852,0.39072,0.28241,0.31141,0.32176,0.24371,0.38657,0.17795,0.44676,0.12186,0.55787,0.10251,0.67824,0.098646,0.75694,0.096712,0.75,0.092843,0.6875,0.10832,0.59722,0.13926,0.50694,0.17988,0.42361,0.23598,0.36111,0.30754,0.30556,0.39845,0.25926,0.48936,0.23843,0.56867,0.25231,0.62282,0.28472,0.6499,0.29861,0.64797,0.28935,0.62089,0.2662,0.56286,0.25231,0.47969,0.25231,0.38491,0.28241,0.30368,0.33565,0.24565,0.40046,0.20503,0.45139,0.17602,0.50694,0.15667,0.5787,0.1412,0.63194,0.13926,0.62037,0.16054,0.55556,0.20503,0.49074,0.26306,0.42361,0.33269,0.34722,0.41006,0.28009,0.4913,0.24306 },
                new double[] { 0.67118,0.38426,0.67118,0.38657,0.67311,0.38657,0.67311,0.38426,0.67311,0.37963,0.65957,0.36574,0.61702,0.3588,0.55706,0.38889,0.49323,0.4537,0.46615,0.52315,0.46228,0.62037,0.47389,0.71759,0.54545,1,0.47969,0.80556,0.44874,0.7037,0.45068,0.59259,0.46809,0.48611,0.51064,0.38426,0.56673,0.2963,0.64603,0.24306,0.74662,0.25926,0.83559,0.34259,0.87621,0.44676,0.87234,0.48148,0.86267,0.44907,0.84913,0.38657,0.82785,0.32407,0.7795,0.25926,0.69826,0.23611,0.60348,0.2662,0.52805,0.32639,0.47776,0.40046,0.44874,0.46528,0.43907,0.53009,0.4294,0.62269,0.44487,0.75231,0.45648,0.84028,0.51257,0.9838,0.5087,0.96991,0.47195,0.81713,0.46422,0.76389,0.44101,0.6412,0.45068,0.54167,0.47776,0.44213,0.53191,0.34259,1 },
                new double[] { 0.83366,0.3588,0.83752,0.37731,0.82979,0.38426,0.83172,0.37731,0.83172,0.41667,0.80077,0.50231,0.73114,0.57639,0.63636,0.63426,0.54545,0.66667,0.47195,0.65278,0.42553,0.61111,0.41006,0.55093,0.41393,0.47685,0.42747,0.40046,0.44681,0.34028,0.46035,0.32639,0.45841,0.36806,0.46422,0.43519,0.49323,0.49769,0.53965,0.5463,0.61315,0.57176,0.70406,0.56944,0.78723,0.54398,0.84139,0.48611,0.8646,0.39815,0.8646,0.31713,0.86654,0.26852,0.87234,0.30093,0.87041,0.38426,0.8472,0.46759,0.78917,0.52546,0.706,0.57407,0.60542,0.61343,0.5029,0.61574,0.43714,0.55324,0.41586,0.45833,0.42553,0.37037,0.44681,0.32176,0.45455,0.35185,0.46035,0.43519,0.4913,0.51157,0.55706,0.56019,0.64603,0.5787,0.74662,0.56713,0.83752,0.52546 },
                new double[] { 0.71567,0.68056,0.71373,0.68056,0.70986,0.68981,0.7118,0.68287,0.71567,0.65741,0.71373,0.65509,0.69439,0.58102,0.6499,0.52083,0.60348,0.49769,0.53965,0.50926,0.45648,0.52083,0.36557,0.54167,0.32302,0.63889,0.25532,0.72917,0.21857,0.80324,0.21083,0.8125,0.22437,0.76389,0.24758,0.65046,0.30948,0.59259,0.34816,0.54861,0.44294,0.5,0.54932,0.48843,0.62476,0.46065,0.68859,0.49306,0.73501,0.56481,0.76983,0.61574,0.78723,0.63426,0.77756,0.61111,0.73888,0.55556,0.67311,0.45833,0.59574,0.47222,0.51064,0.49306,0.40426,0.51852,0.33849,0.57407,0.29594,0.59259,0.26306,0.68519,0.23598,0.75231,0.22437,0.7662,0.23017,0.74306,0.24758,0.67824,0.30174,0.61111,0.35397,0.57639,0.41006,0.52315,0.4913,0.48611,0.55899,0.4838 },
                new double[] { 0.77563,0.60417,0.77563,0.60417,0.7795,0.59722,0.75629,0.53935,0.70793,0.44444,0.64217,0.39815,0.54352,0.43519,0.4294,0.4838,0.37524,0.53935,0.32882,0.6088,0.31721,0.73148,0.31335,0.75694,0.31335,0.75,0.31141,0.64352,0.34816,0.56713,0.37718,0.51852,0.44101,0.48148,0.52998,0.45833,0.61122,0.45602,0.65957,0.43981,0.70019,0.49537,0.72921,0.53009,0.73308,0.64352,0.74662,0.64583,0.74468,0.60185,0.73308,0.50231,0.70213,0.48148,0.6499,0.44676,0.58607,0.44213,0.5087,0.43056,0.43133,0.4537,0.35977,0.49306,0.33075,0.54861,0.30368,0.60185,0.28627,0.69907,0.29207,0.78704,0.28433,0.74306,0.29207,0.62963,0.32689,0.56481,0.3675,0.51852,0.45648,0.45833,0.54545,0.4537,0.61509,0.47685,0.66344,0.46759,0.7118,0.53472 },
            };

            // Specify a initial normal distribution for the samples.
            var density = new NormalDistribution();

            // Creates a continuous hidden Markov Model with two states organized in a forward
            //  topology and an underlying univariate ssNormal distribution as probability density.
            var model = new HiddenMarkovModel<NormalDistribution, double>(new Ergodic(12), density);

            // Configure the learning algorithms to train the sequence classifier until the
            // difference in the average log-likelihood changes only by as little as 0.0001
            var teacher = new BaumWelchLearning<NormalDistribution, double>(model)
            {
                Tolerance = 0.001,
                MaxIterations = 0,
            };

            // Fit the model
            teacher.Learn(sequences);

            double logLikelihood = teacher.LogLikelihood;

            // See the log-probability of the sequences learned
            double a1 = model.LogLikelihood(new[] { 0.77756, 0.63426, 0.77563, 0.63426, 0.77369, 0.61806, 0.75822, 0.50694, 0.71373, 0.40509, 0.65184, 0.38426, 0.56286, 0.36574, 0.46228, 0.39352, 0.38878, 0.43519, 0.34623, 0.4838, 0.32882, 0.53472, 0.31141, 0.57176, 0.29207, 0.61806, 0.29594, 0.7338, 0.29014, 0.72454, 0.29981, 0.58796, 0.34236, 0.51852, 0.42166, 0.45139, 0.51451, 0.41667, 0.58994, 0.43287, 0.64797, 0.44444, 0.69246, 0.45602, 0.74662, 0.51852, 0.80464, 0.56713, 0.84139, 0.61574, 0.84526, 0.60648, 0.81625, 0.55556, 0.75242, 0.46991, 0.66731, 0.40972, 0.58027, 0.43056, 0.49323, 0.4213, 0.40619, 0.44676, 0.33849, 0.51157, 0.29594, 0.55787, 0.23017, 0.59954, 0.17795, 0.68519, 0.16248, 0.71759, 0.17408, 0.7037, 0.23598, 0.58102, 0.32108, 0.52778, 0.38298, 0.46991, 0.46422, 0.4375, 0.55899, 0.44213, 0.6325, 0.46296, 0.68085, 0.4838 });
            double a2 = model.LogLikelihood(new[] { 0.74468, 0.58565, 0.74468, 0.59028, 0.74662, 0.58333, 0.73501, 0.55787, 0.7118, 0.47222, 0.65764, 0.43287, 0.59188, 0.45139, 0.5029, 0.45139, 0.41199, 0.47685, 0.36557, 0.53704, 0.31528, 0.5625, 0.27466, 0.67593, 0.24952, 0.71759, 0.25145, 0.71991, 0.26112, 0.6713, 0.30561, 0.59028, 0.3559, 0.50694, 0.45455, 0.46296, 0.53191, 0.47222, 0.60542, 0.47454, 0.65764, 0.46991, 0.70793, 0.55324, 0.73888, 0.62037, 0.74855, 0.64583, 0.74662, 0.64352, 0.73308, 0.59722, 0.69439, 0.49769, 0.62863, 0.4537, 0.52998, 0.43519, 0.41973, 0.44676, 0.32108, 0.46296, 0.24371, 0.52546, 0.2089, 0.63889, 0.17988, 0.7338, 0.16634, 0.79167, 0.16828, 0.78704, 0.18762, 0.73611, 0.21857, 0.625, 0.27273, 0.52778, 0.36557, 0.46528, 0.47002, 0.45833, 0.56286, 0.45602, 0.62282, 0.4537, 0.66538, 0.48148, 0.71567, 0.53935 }); 
            
            // See the probability of an unrelated sequence
            double a3 = model.LogLikelihood(new[] { 0.46422, 0.63889, 0.46422, 0.63657, 0.46422, 0.63657, 0.46615, 0.63194, 0.46228, 0.63889, 0.46228, 0.62963, 0.46228, 0.625, 0.46035, 0.60185, 0.45455, 0.56019, 0.44294, 0.50694, 0.4294, 0.44676, 0.41779, 0.3912, 0.40426, 0.34259, 0.39652, 0.30556, 0.38491, 0.27778, 0.37524, 0.25926, 0.36557, 0.25463, 0.3559, 0.25231, 0.3501, 0.26389, 0.34623, 0.28009, 0.33656, 0.30556, 0.33075, 0.35648, 0.33656, 0.42593, 0.34816, 0.50463, 0.36364, 0.55324, 0.38685, 0.60185, 0.37911, 0.64352, 0.37718, 0.66204, 0.38104, 0.66667, 0.38491, 0.66435, 0.39265, 0.65509, 0.40426, 0.63889, 0.40812, 0.62037, 0.40426, 0.60417, 0.39845, 0.57407, 0.40039, 0.50694, 0.40426, 0.43287, 0.40039, 0.37269, 0.39265, 0.32639, 0.38685, 0.29398, 0.38104, 0.27778, 0.37524, 0.27315, 0.36944, 0.28009, 0.36364, 0.30093, 0.35977, 0.34491 });

            double likelihood = Math.Exp(logLikelihood);
            a1 = Math.Exp(a1);
            a2 = Math.Exp(a2);
            a3 = Math.Exp(a3);

            Console.WriteLine("Prob a1: " + a1.ToString("0.000"));
            Console.WriteLine("Prob a2: " + a2.ToString("0.000"));
            Console.WriteLine("Prob a3: " + a3.ToString("0.000"));
        }

        static double[][][] Ler_Arquivo_Treino()
        {
            System.IO.StreamReader arq = new System.IO.StreamReader(@"C:\movement_libras.csv");
            double[][][] sequencias = new double[24][][];

            string linha = "";
            bool parada = false;
            while (!parada)
            {
                for (int j = 0; j < 24; j++)
                {
                    linha = arq.ReadLine();
                    if (linha != null)
                    {
                        double[][] instancia = new double[45][];
                        string[] dados = linha.Split(',');
                        int cont = 0;
                        for (int i = 0; i < dados.Length - 1; i = i + 2)
                        {
                            double p1 = double.Parse(dados[i].Replace('.', ','));
                            double p2 = double.Parse(dados[i + 1].Replace('.', ','));
                            double[] ponto = new double[] { p1, p2 };
                            instancia[cont] = ponto;
                            cont++;
                        }
                        sequencias[j] = instancia;
                    }
                    else
                    {
                        parada = true;
                        break;
                    }
                }
                break;
            }

            //for (int i = 0; i < sequencias.Length; i++)
            //{
            //    Console.WriteLine("Instancia " + (i + 1));
            //    for (int j = 0; j < sequencias[i].Length; j++)
            //    {
            //        Console.Write("P1: " + sequencias[i][j][0] + "   P2: " + sequencias[i][j][1]);
            //        Console.WriteLine();
            //    }
            //    Console.WriteLine();
            //}
            return sequencias;
        }

        static double[][][] Ler_Arquivo_Teste()
        {
            System.IO.StreamReader arq = new System.IO.StreamReader(@"C:\movement_libras_1.data");
            double[][][] sequencias = new double[3][][];

            string linha = "";
            bool parada = false;
            while (!parada)
            {
                // 3 instancias de teste somente da classe 1
                for (int j = 0; j < 3; j++)
                {
                    linha = arq.ReadLine();
                    if (linha != null)
                    {
                        double[][] instancia = new double[45][];
                        string[] dados = linha.Split(',');
                        int cont = 0;
                        for (int i = 0; i < dados.Length - 1; i = i + 2)
                        {
                            double p1 = double.Parse(dados[i].Replace('.', ','));
                            double p2 = double.Parse(dados[i + 1].Replace('.', ','));
                            double[] ponto = new double[] { p1, p2 };
                            instancia[cont] = ponto;
                            cont++;
                        }
                        sequencias[j] = instancia;
                    }
                    else
                    {
                        parada = true;
                        break;
                    }
                }
                break;
            }
            return sequencias;
        }
    }
}