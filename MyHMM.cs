﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.Statistics.Models.Markov;
using Accord.Statistics.Models.Markov.Learning;
using Accord.Statistics.Models.Markov.Topology;
using Accord.Statistics.Distributions.Univariate;

namespace HMM_Projeto
{
    class MyHMM
    {
        private int states;
        private int symbols;

        public MyHMM(int states, int symbols)
        {
            this.states = states;
            this.symbols = symbols;
        }

        public double[] LearnAndDecide(int[][] input, int[][] teste)
        {
            try
            {

                // Create a new Hidden Markov Model with 3 states and
                //  a generic discrete distribution with two symbols
                var hmm = HiddenMarkovModel.CreateDiscrete(this.states, this.symbols);

                // Try to fit the model to the data until the difference in
                //  the average log-likelihood changes only by as little as 0.0001
                var teacher = new BaumWelchLearning<GeneralDiscreteDistribution, int>(hmm)
                {
                    Tolerance = 0.0001,
                    MaxIterations = 0
                };

                // Learn the model
                teacher.Learn(input);

                //double ll = Math.Exp(teacher.LogLikelihood);

                double[] prob = new double[teste.Length];
                for (int i = 0; i < teste.Length; i++)
                {
                    prob[i] = Math.Exp(hmm.LogLikelihood(teste[i]));
                    Console.WriteLine("Probabilidade para " + (i+1) + ": " + prob[i].ToString("0.00000000000000000000"));
                }

                return prob;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }   

    }
}
